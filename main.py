from fastapi import FastAPI
from BasicApp.BasicApp import BasicAppRouter
from contextlib import asynccontextmanager
from starlette.middleware.cors import CORSMiddleware

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(BasicAppRouter,prefix="/ba",tags=['basicApp'])

