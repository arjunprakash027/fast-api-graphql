from typing import Union
from fastapi import FastAPI, Header, Depends,Body,Query,BackgroundTasks, APIRouter
from pydantic import BaseModel
import time
from contextlib import asynccontextmanager
import re
from .schema1 import graphql_app

BasicAppRouter = APIRouter()


BasicAppRouter.include_router(graphql_app,prefix="/graphql")
@BasicAppRouter.get("/")
async def BAHome():
    return "Welcome to me BasicApp graphql test server"


