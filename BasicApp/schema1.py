import strawberry
from sqlalchemy import select

from fastapi import FastAPI
from strawberry.fastapi import GraphQLRouter
from . import models, db_connection

todo: list[str] = []

@strawberry.type
class Post:
    id: strawberry.ID
    title: str
    description: str

    @classmethod
    def marshal(cls,model: models.Post) -> list[models.Post]:
        # print(cls(id=strawberry.ID(str(model.id)), title=model.title, description=model.description))
        return cls(id=strawberry.ID(str(model.id)), title=model.title, description=model.description)

@strawberry.type
class Query:
    @strawberry.field
    def all_todo(self) -> list[Post]:
        with db_connection.get_db() as db:
            sql = select(models.Post)
            db_post = db.execute(sql).scalars().unique().all()
        
        return [Post.marshal(post) for post in db_post]
    

@strawberry.type
class Mutation:
    @strawberry.field
    def add_todo(self,task:str) -> str:
        todo.append(task)
        return task

schema = strawberry.Schema(query=Query,mutation=Mutation)

graphql_app = GraphQLRouter(schema)

